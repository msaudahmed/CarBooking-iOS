//
//  FileManager.swift
//  CarBooking
//
//  Created by Saud Ahmed on 12.01.18.
//  Copyright © 2018 Kupferwerk. All rights reserved.
//

import Foundation

/**
 The purpose of the `NetworkManager` is to save and load Plist from the Documents folder inside applicaton
 */
public class PListManager
{
    /**
     Save array of dictionaries to a plist
     */
    static func saveDictionaryToPlist(withDictionary dictionary: [String: Any])
    {
        copyPlistFromMainBundleToDocuments()
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = paths.appending("/booking-details.plist")
        
        if let plistArray = NSMutableArray(contentsOfFile: path)
        {
            plistArray.add(dictionary)
            plistArray.write(toFile: path, atomically: true)
        }
        
        NetworkManager.syncPlistWithBackEnd()
    }
    
    /**
     Loads array of dictionaries from a plist
     */
    static func loadArrayFromPlist() -> Array<[String:Any]>?
    {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = paths.appending("/booking-details.plist")
        
        let readPlistArray = NSMutableArray(contentsOfFile: path)
        
        if let array = readPlistArray
        {
            return array as? Array<[String:Any]>
        }
        else
        {
            return nil
        }
    }
    
    private static func copyPlistFromMainBundleToDocuments()
    {
        let rootPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let plistPathInDocument = rootPath.appending("/booking-details.plist")
        
        if !FileManager.default.fileExists(atPath:plistPathInDocument)
        {
            let plistPathInBundle = Bundle.main.path(forResource: "booking-details", ofType: "plist") as String!
            do {
                try FileManager.default.copyItem(atPath: plistPathInBundle!, toPath: plistPathInDocument)
            }catch{
                print("Error occurred while copying file to document \(error)")
            }
        }
    }
}
