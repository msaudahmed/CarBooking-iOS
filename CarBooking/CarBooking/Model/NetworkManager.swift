//
//  NetworkManager.swift
//  CarBooking
//
//  Created by Saud Ahmed on 12.01.18.
//  Copyright © 2018 Kupferwerk. All rights reserved.
//

import Foundation

/**
 The purpose of the `NetworkManager` is to sync plist with backend
 */
public class NetworkManager
{
    /**
     Sync plist by serializing it into json and posting it using HTTP post
     */
    static func syncPlistWithBackEnd()
    {
        let url = URL(string: "http://job-applicants-dummy-api.kupferwerk.net.s3.amazonaws.com/api/")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        
        let postString = serializePlistToJsonString()
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else
            {
                // check for fundamental networking error
                print("error=\(error.debugDescription)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200
            {
                // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response.debugDescription)")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("response = \(responseString.debugDescription)")
        }
        
        task.resume()
    }
    
    /**
     Serialize plist by serializing plist to json string
     */
    static func serializePlistToJsonString() -> String
    {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = paths.appending("/booking-details.plist")
        let fileUrl = URL(fileURLWithPath: path)
        
        var convertedString = "";
        
        do {
            let data = try Data(contentsOf:fileUrl)
            let dict = try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as! [[String:Any]]
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            
            convertedString = String(data: jsonData, encoding: String.Encoding.utf8)! // the data will be converted to the string
            print(convertedString)
            
            // jsondata  your required data
        } catch {
            print(error)
        }
        
        return convertedString
    }
}
