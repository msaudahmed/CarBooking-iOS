//
//  LocalNotificationManager.swift
//  CarBooking
//
//  Created by Saud Ahmed on 12.01.18.
//  Copyright © 2018 Kupferwerk. All rights reserved.
//

import Foundation
import UserNotifications

/**
 The purpose of the `LocalNotificationManager` is to trigger local notfications
 */
public class LocalNotificationManager
{
    /**
     Trigger local Notifications
     
     - Parameter title: Title of the notifications
     - Parameter body: body of the notifications
     - Parameter id: unique id of the notifications
     - Parameter date: date to trigger notification
     */
    static func triggerLocalNotification(title: String, body: String, id: String, date: Date)
    {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
        
        let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,], from: date)

        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate,
                                                    repeats: false)
        
        let identifier = id
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.add(request, withCompletionHandler: { (error) in
            if let error = error {
                print("Something went wrong")
            }
            else{
                print("notification scheduled")
            }
        })
    }
}
