//
//  DateHelper.swift
//  CarBooking
//
//  Created by Saud Ahmed on 12.01.18.
//  Copyright © 2018 Kupferwerk. All rights reserved.
//

import Foundation
/**
 The purpose of the `DateHelper` is to give current date and next date from the calendar
 */
public class DateHelper
{
    /**
     Get current date as a string
     */
    static func getCurrentDate() -> String
    {
        let date = Date()
        let formatter = DateFormatter()
        
        //Give the format you want to the formatter
        formatter.dateFormat = "dd.MM.yyyy"
        
        //Get the result string:
        let result = formatter.string(from: date)
        
        return result
    }
    
    /**
     Get tomorrows date as a string
     */
    static func getTomorrowtDateString() -> String
    {
        let dateString = getCurrentDate()
        
        let dateFormatter = DateFormatter()
        
        var result : String = "";
        
        //Give the format you want to the formatter
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        let presentDate = dateFormatter.date(from: dateString)
        
        if let presentDate = presentDate
        {
            let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: presentDate)
            
            if let tomorrow = tomorrow
            {
                result = dateFormatter.string(from: tomorrow)
            }
        }
        
        return result
    }
    
    /**
     Get tomorrows date
     */
    static func getTomrrowDate() -> Date?
    {
        let dateString = getCurrentDate()
        
        let dateFormatter = DateFormatter()
        
        var tomorrow : Date? = nil
        
        //Give the format you want to the formatter
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        let presentDate = dateFormatter.date(from: dateString)
        
        if let presentDate = presentDate
        {
            tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: presentDate)
        }
        
        return tomorrow
    }
}
