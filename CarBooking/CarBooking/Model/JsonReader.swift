//
//  JsonReader.swift
//  CarBooking
//
//  Created by Saud Ahmed on 09.01.18.
//  Copyright © 2018 Kupferwerk. All rights reserved.
//

import Foundation

// UI-independent representation of JsonReader

/**
 
 The purpose of the `JsonReader` is to read a json from http address as a GET request. Parse received json array as data source to pass to a controller. A controller is used to update UI of table view according to data source passed as a data model.
 
 */

public class JsonReader
{
    /**
     Queries an http address with a GET request to receive a file. The received file is in a Json format contains vehicles data. Vehicles data is parsed as a data structure of Array. And notifies a controller about it via code block to update a **Table View**
     
     ### Usage Example: ###
     ````
     let jsonReader = JsonReader()
     jsonReader.fetchDataFromRemoteJsonForCarList()
     
     ````
     */
    
    private func fetchDataFromRemoteJsonForCarList(withUrl urlString: String, callback: @escaping (String?, Any?)->Void)
    {
        let url = URL(string: urlString)
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if let data = data {
                do {
                    // Convert the data to JSON
                    let jsonSerialized = try JSONSerialization.jsonObject(with: data, options: []) as? [[String : Any]]
                    
                    if let jsonArray = jsonSerialized{
                        
                        let dataSource = self.createDataStructure(fromJsonArray: jsonArray)
                        
                        callback("ok", dataSource)
                    }
                }  catch let error as NSError {
                    callback("error", error.localizedDescription)
                }
            } else if let error = error {
                callback("error", error.localizedDescription)
            }
        }
        
        task.resume()
    }
    
    /**
     Queries an http address with a GET request to receive a file. The received file is in a Json format contains vehicle detail. Vehicle data is parsed as a data structure of dictionary. And notifies a controller about it via code block to update a **Table View**
     
     ### Usage Example: ###
     ````
     let jsonReader = JsonReader()
     jsonReader.fetchDataFromRemoteJsonForCarDetail()
     
     ````
     */
    
    private func fetchDataFromRemoteJsonForCarDetail(withUrl urlString: String, callback: @escaping (String?, Any?)->Void)
    {
        let url = URL(string: urlString)
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if let data = data {
                do {
                    // Convert the data to JSON
                    let jsonSerialized = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                    
                    if let jsonDictionary = jsonSerialized{
                        
                        let dataSource = self.createDataStructure(fromJsonDictionary: jsonDictionary)
                            
                        callback("ok", dataSource)
                    }
                }  catch let error as NSError {
                    callback("error", error.localizedDescription)
                }
            } else if let error = error {
                callback("error", error.localizedDescription)
            }
        }
        
        task.resume()
    }
    /**
     Fetches a list of cars with id, short name and description
     */
    
    public func fetchCarList(callback: @escaping (String?, Any?)->Void)
    {
        let urlString = "http://job-applicants-dummy-api.kupferwerk.net.s3.amazonaws.com/api/cars.json"
        
        self.fetchDataFromRemoteJsonForCarList(withUrl: urlString, callback: callback)
    }
    
    /**
     Fetches a detail of a car with id. Detail includes id, name, short description, full description and image
     */
    public func fetchCarDetail(withId id: Int?, callback: @escaping (String?, Any?)->Void)
    {
        if let id = id
        {
            let urlString = "http://job-applicants-dummy-api.kupferwerk.net.s3.amazonaws.com/api/cars/\(id).json"
            
            self.fetchDataFromRemoteJsonForCarDetail(withUrl: urlString, callback: callback)
        }
    }
    
    /**
     Creates a data structure of Array by taking a parsed json array as parameter
     
     ### Usage Example: ###
     ````
     let dataSource = createDataStructure(fromJsonArray: array)
     
     ````
     - Parameter jsonArray: It is an array of Dictionary.
     - Returns: Array of dictionaries with String as key and Any as values
     */
    private func createDataStructure(fromJsonArray jsonArray: [Dictionary<String, Any>] ) -> [[String: Any]?]
    {
        var dataSource = [[String: Any]?]()
        
        for element in jsonArray
        {
            var dictionary = element
            
            let idInteger = dictionary["id"] as? Int ?? 0
            let nameString = dictionary["name"] as? String ?? ""
            let shortDescriptionString = dictionary["shortDescription"] as? String ?? ""
            
            dictionary.updateValue(idInteger, forKey: "id")
            dictionary.updateValue(nameString, forKey: "name")
            dictionary.updateValue(shortDescriptionString, forKey: "shortDescription")
            
            dataSource.append(dictionary)
        }
        
        // Sorting case sensitive
        let data = dataSource.sorted{($0?["name"] as! String).compare(($1?["name"] as! String), options: .caseInsensitive) == .orderedAscending }
        
        return data;
    }
    
    /**
     Creates a data structure of Array by taking a parsed json array as parameter
     
     ### Usage Example: ###
     ````
     let dataSource = createDataStructure(fromJsonArray: array)
     
     ````
     - Parameter jsonArray: It is an array of Dictionary.
     - Returns: Array of dictionaries with String as key and Any as values
     */
    private func createDataStructure(fromJsonDictionary jsonDictionary: Dictionary<String, Any> ) -> [String: Any]?
    {
        var dictionary = [String: Any]()
        
        let idInteger = jsonDictionary["id"] as? Int ?? 0
        let nameString = jsonDictionary["name"] as? String ?? ""
        let shortDescriptionString = jsonDictionary["shortDescription"] as? String ?? ""
        let descriptionString = jsonDictionary["description"] as? String ?? ""
        var imageString = ""
        
        if let image = jsonDictionary["image"] as? String? ?? ""
        {
            imageString = "http://job-applicants-dummy-api.kupferwerk.net.s3.amazonaws.com/api/" + image
        }
        
        dictionary.updateValue(idInteger, forKey: "id")
        dictionary.updateValue(nameString, forKey: "name")
        dictionary.updateValue(shortDescriptionString, forKey: "shortDescription")
        dictionary.updateValue(descriptionString, forKey: "description")
        dictionary.updateValue(imageString, forKey: "image")
        
        return dictionary;
    }
}
