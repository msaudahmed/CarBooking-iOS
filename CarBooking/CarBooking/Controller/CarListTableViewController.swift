//
//  CarListTableViewController.swift
//  CarBooking
//
//  Created by Saud Ahmed on 09.01.18.
//  Copyright © 2018 Kupferwerk. All rights reserved.
//

import UIKit

/**
 The purpose of the `CarListTableViewController` is to provide a user interface to show list of cars
 
 There's a matching scene in the *Main.storyboard* file, and in that scene there is a table view controller with Navigation Controller. Go to Interface Builder for details.
 
 The `CarListTableViewController` class is a subclass of the `UITableViewController`, and it conforms to the `UITableViewDelegate, UITableViewDataSource` protocol.
 */

class CarListTableViewController: UITableViewController, UISplitViewControllerDelegate {

    /// Data model to be used to create table view
    private var carListDataSource = [Dictionary<String, Any>?]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.splitViewController?.delegate = self
        self.splitViewController?.preferredDisplayMode = UISplitViewControllerDisplayMode.allVisible
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.refreshControl?.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        
        let jsonReader = JsonReader()
        jsonReader.fetchCarList()
        { (error: String?, data: Any?) -> Void in
            
            if error == "ok"
            {
                self.recreateTableView(data: data)
            }
            else if error == "error"
            {
                self.showErrorAlert(data: data)
            }
        }
    }
    
    // MARK: - Handle response methods
    
    /**
     Recreate a table view when reponse data is received
     
     - Parameter data: Contains responce data
     */
    private func recreateTableView(data: Any?) -> Void
    {
        guard let data = data,
            let dataArray  = data as? [Dictionary<String, Any>?] else {
                print("No data found")
                return
        }
        
        //Received dataArrays on global queue so reload table view on Main queue
        DispatchQueue.main.async {
            self.refreshControl?.endRefreshing()
            
            self.carListDataSource = dataArray
            self.tableView.reloadData()
        }
    }
    
    /**
     Show error alert when error is received
     
     - Parameter data: Contains responce data as error description
     */
    private func showErrorAlert(data: Any?) -> Void
    {
        guard let data = data,
            let errorString  = data as? String else {
                print("No error found")
                return
        }
        
        //Display Alert View on Main Queue
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    // MARK: - Table view refresh handler
    
    /**
     Upon table view pull down this method is called to fetch json data to update table view
     
     - Parameter refreshControl: Refresh control that invokes this method.
     */
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        
        let jsonReader = JsonReader()
        jsonReader.fetchCarList()
            { (error: String?, data: Any?) -> Void in
                
                if error == "ok"
                {
                    self.recreateTableView(data: data)
                }
                else if error == "error"
                {
                    self.showErrorAlert(data: data)
                }
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return carListDataSource.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        // Configure the cell...
        
        let dictionary = carListDataSource[indexPath.row]
        
        if let cell = cell as? CarListTableViewCell {
            cell.dictionary = dictionary
        }
        
        return cell
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "carDetailSegue"
        {
            let indexPath = tableView.indexPath(for: sender as! CarListTableViewCell)
            
            if let indexPath = indexPath
            {
                if let carDetailsViewController = segue.destination.contents as? CarDetailsViewController{
                    carDetailsViewController.dictionary = self.carListDataSource[indexPath.row]
                    carDetailsViewController.title = NSLocalizedString("CAR_DETAILS", comment: "localized")
                }
            }
        }
    }
    
    // MARK: - SplitViewController Delegate
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        if primaryViewController.contents == self
        {
            if secondaryViewController.contents is CarDetailsViewController
            {
                return true;
            }
        }
        
        return false
    }
}

extension UIViewController
{
    var contents: UIViewController
    {
        if let navcon = self as? UINavigationController{
            return navcon.visibleViewController ?? self
        }
        else
        {
            return self
        }
    }
}
