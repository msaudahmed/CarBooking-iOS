//
//  CarDetailsViewController.swift
//  CarBooking
//
//  Created by Saud Ahmed on 11.01.18.
//  Copyright © 2018 Kupferwerk. All rights reserved.
//

import UIKit

/**
 The purpose of the `CarDetailsViewController` is to show details of a car and booking option inside a view controller
 
 There's a matching scene in the *Main.storyboard* file, and in that scene there is a car details view controller with Navigation Controller. Go to Interface Builder for details.
 
 The `CarDetailsViewController` class is a subclass of the `UIViewController`.
 */

class CarDetailsViewController: UIViewController {
    
    /// Dictionary to store single car data.
    public var dictionary : Dictionary<String, Any>?
    {
        didSet{
            updateUI()
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var shortDescriptionTextView: UITextView!
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var durationLabel: UILabel!
    
    let dateAndTimePicker = UIDatePicker()
    let toolBar = UIToolbar()
    
    var selectedDate = DateHelper.getTomorrowtDateString()
    var selectedTime = "9:00 am"
    var duration: Int = 1
    var carId : Int = 0;
    var nameString : String = ""
    var shortDescriptionString : String = ""
    var descriptionString : String = ""
    var imageString : String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        dateAndTimeLabel.text = "Date: " + selectedDate + " " + "Time: " + selectedTime
        
        addDatePicker()
    }
    
    // MARK: - Update UI
    
    /**
     Updates UI for the values present against keys in dictionary
     */
    private func updateUI() {
        
        // whenever our public API dictionary is set
        // we just update our outlets using this method
        
        self.carId = dictionary?["id"] as? Int ?? 0
        
        let jsonReader = JsonReader()
        jsonReader.fetchCarDetail(withId: self.carId) { (error: String?, data: Any?) -> Void in
            
            if error == "ok"
            {
                guard let data = data,
                    let dataDicitionary  = data as? Dictionary<String, Any> else {
                        print("No data found")
                        return
                }
                
                self.nameString = dataDicitionary["name"] as? String ?? ""
                self.shortDescriptionString = dataDicitionary["shortDescription"] as? String ?? ""
                self.descriptionString = dataDicitionary["description"] as? String ?? ""
                
                DispatchQueue.main.async() {[weak self] in //Memory cycle here
                    self?.titleLabel.text = self?.nameString
                    self?.shortDescriptionTextView.text = self?.shortDescriptionString
                    self?.textView.text = self?.descriptionString;
                    self?.imageView.image = UIImage(named: "car-placeholder")
                    
                    self?.view.isHidden = false
                }
                
                self.imageString = dataDicitionary["image"] as? String? ?? ""
                
                if let imageString = self.imageString
                {
                    let url = URL(string: imageString)
                    
                    let queue = DispatchQueue.global(qos: .userInitiated)
                    
                    queue.async{ [weak self] in //Memory cycle here
                        if let imageData = try? Data(contentsOf: url!) {
                            DispatchQueue.main.async() {
                                self?.imageView?.image = UIImage(data: imageData)
                            }
                        }
                    }
                    
                    queue.resume()
                }
            }
            else if error == "error"
            {
                self.showErrorAlert(data: data)
            }
        }
        
    }
    
    /**
     Show error alert when error is received
     
     - Parameter data: Contains responce data as error description
     */
    private func showErrorAlert(data: Any?) -> Void
    {
        guard let data = data,
            let errorString  = data as? String else {
                print("No error found")
                return
        }
        
        //Display Alert View on Main Queue
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     Adds date and time picker to choose start date and time
     */
    func addDatePicker(){
        // DatePicker
        self.dateAndTimePicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 200)
        self.dateAndTimePicker.backgroundColor = UIColor.white
        self.dateAndTimePicker.datePickerMode = UIDatePickerMode.dateAndTime
        self.dateAndTimePicker.minimumDate = DateHelper.getTomrrowDate()
        view.addSubview(self.dateAndTimePicker)
        
        dateAndTimePicker.translatesAutoresizingMaskIntoConstraints = false

        var leadingConstraint = NSLayoutConstraint(item: dateAndTimePicker, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
        var trailingConstraint = NSLayoutConstraint(item: dateAndTimePicker, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
        var bottomConstraint = NSLayoutConstraint(item: dateAndTimePicker, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraints([leadingConstraint, trailingConstraint, bottomConstraint])

        // ToolBar
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20)
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: NSLocalizedString("DONE", comment: "localized"), style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("CANCEL", comment: "localized"), style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        self.view.addSubview(toolBar)
        
        toolBar.translatesAutoresizingMaskIntoConstraints = false

        leadingConstraint = NSLayoutConstraint(item: toolBar, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
        trailingConstraint = NSLayoutConstraint(item: toolBar, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
        bottomConstraint = NSLayoutConstraint(item: toolBar, attribute: .bottom, relatedBy: .equal, toItem: dateAndTimePicker, attribute: .top, multiplier: 1, constant: 0)
        view.addConstraints([leadingConstraint, trailingConstraint, bottomConstraint])

        
        self.dateAndTimePicker.isHidden = true
        self.toolBar.isHidden = true
    }
}

extension CarDetailsViewController
{
    /**
     Handle button press to show date and time picker
     
     - Parameter sender: UIButton reference for which button is pressed
     */
    @objc func doneClick(sender: UIBarButtonItem!) {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat =  "dd.MM.yyyy"
        self.selectedDate = dateFormatter.string(from: dateAndTimePicker.date);
        
        dateFormatter.dateFormat =  "h:mm a"
        self.selectedTime = dateFormatter.string(from: dateAndTimePicker.date);
        
        dateAndTimeLabel.text = "Date: " + self.selectedDate + " " + "Time: " + self.selectedTime
        self.dateAndTimePicker.isHidden = true
        self.toolBar.isHidden = true
    }
    
    /**
     Handle button press to cancel picking date and time
     
     - Parameter sender: UIButton reference for which button is pressed
     */
    @objc func cancelClick(sender: UIBarButtonItem!) {
        self.dateAndTimePicker.isHidden = true
        self.toolBar.isHidden = true
    }
    
    /**
     Handle button press to show date and time
     
     - Parameter sender: UIButton reference for which button is pressed
     */
    @IBAction func pickButtonPressed(_ sender: UIButton) {
        self.dateAndTimePicker.isHidden = false
        self.toolBar.isHidden = false
    }
    
    /**
     Handle slider value change to save duration in days
     
     - Parameter sender: UISlider reference
     */
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        
        self.duration = Int(sender.value)
        
        durationLabel.text = "\(duration) days"
    }
    
    /**
     Handle button press to book a car
     
     - Parameter sender: UIButton reference for which button is pressed
     */
    @IBAction func bookCar(_ sender: UIButton) {
        var dictionary = [String: Any]()
        
        dictionary.updateValue(self.selectedDate, forKey: "date")
        dictionary.updateValue(self.selectedTime, forKey: "time")
        dictionary.updateValue(self.duration, forKey: "duration")
        dictionary.updateValue(self.carId, forKey: "id")
        dictionary.updateValue(self.nameString, forKey: "name")
        dictionary.updateValue(self.shortDescriptionString, forKey: "shortDescription")
        dictionary.updateValue(self.descriptionString, forKey: "description")
        
        if let imageString = self.imageString
        {
            dictionary.updateValue(imageString, forKey: "image")
        }
        
        PListManager.saveDictionaryToPlist(withDictionary: dictionary)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy h:mm a"
        formatter.timeZone = TimeZone.autoupdatingCurrent
        
        let date = formatter.date(from: self.selectedDate + " " + self.selectedTime)
        
        LocalNotificationManager.triggerLocalNotification(title: self.nameString, body: "Booking time started", id: String(self.carId), date: date!)
        
        //Display Alert View on Main Queue
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Car Booked", message: self.nameString, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
