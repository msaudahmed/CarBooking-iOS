//
//  BookedCarListTableViewController.swift
//  CarBooking
//
//  Created by Saud Ahmed on 12.01.18.
//  Copyright © 2018 Kupferwerk. All rights reserved.
//

import UIKit

/**
 The purpose of the `BookedCarListTableViewController` is to provide a user interface to show list of booked cars
 
 There's a matching scene in the *Main.storyboard* file, and in that scene there is a table view controller with Navigation Controller. Go to Interface Builder for details.
 
 The `BookedCarListTableViewController` class is a subclass of the `UITableViewController`, and it conforms to the `UITableViewDelegate, UITableViewDataSource` protocol.
 */

class BookedCarListTableViewController: UITableViewController, UISplitViewControllerDelegate {

    /// Data model to be used to create table view
    private var carbookedDataSource = [Dictionary<String, Any>?]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.splitViewController?.delegate = self
        self.splitViewController?.preferredDisplayMode = UISplitViewControllerDisplayMode.allVisible
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.refreshControl?.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        
        let array = PListManager.loadArrayFromPlist()
        
        if let data = array {
            self.recreateTableView(data: data)
        }
    }
    
    // MARK: - Handle response methods
    
    /**
     Recreate a table view when reponse data is received
     
     - Parameter data: Contains response data
     */
    private func recreateTableView(data: Any?) -> Void
    {
        guard let data = data,
            let dataArray  = data as? [Dictionary<String, Any>?] else {
                print("No data found")
                return
        }
        
        //Received dataArrays on global queue so reload table view on Main queue
        DispatchQueue.main.async {
            self.refreshControl?.endRefreshing()
            
            // Sorting case sensitive
            let data = dataArray.sorted{($0?["name"] as! String).compare(($1?["name"] as! String), options: .caseInsensitive) == .orderedAscending }
            
            self.carbookedDataSource = data
            self.tableView.reloadData()
        }
    }

    // MARK: - Table view refresh handler
    
    /**
     Upon table view pull down this method is called to fetch json data to update table view
     
     - Parameter refreshControl: Refresh control that invokes this method.
     */
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        
        let array = PListManager.loadArrayFromPlist()
        
        self.recreateTableView(data: array)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return carbookedDataSource.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookedcell", for: indexPath)
        
        // Configure the cell...
        
        let dictionary = carbookedDataSource[indexPath.row]
        
        if let cell = cell as? CarListTableViewCell {
            cell.dictionary = dictionary
        }
        
        return cell
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "bookDetailSegue"
        {
            let indexPath = tableView.indexPath(for: sender as! CarListTableViewCell)
            
            if let indexPath = indexPath
            {
                if let bookedCarDetailsViewController = segue.destination.contents as? BookedCarDetailsViewController{
                    bookedCarDetailsViewController.dictionary = self.carbookedDataSource[indexPath.row]
                    bookedCarDetailsViewController.title = NSLocalizedString("BOOKING_DETAILS", comment: "localized")
                }
            }
        }
    }
    
    // MARK: - SplitViewController Delegate
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        if primaryViewController.contents == self
        {
            if secondaryViewController.contents is BookedCarDetailsViewController
            {
                return true;
            }
        }
        
        return false
    }
}
