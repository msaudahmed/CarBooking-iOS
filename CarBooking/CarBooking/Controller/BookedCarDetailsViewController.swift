//
//  BookedCarDetailsViewController.swift
//  CarBooking
//
//  Created by Saud Ahmed on 12.01.18.
//  Copyright © 2018 Kupferwerk. All rights reserved.
//

import UIKit

/**
 The purpose of the `BookedCarDetailsViewController` is to show details of a booked car and booking details inside a view controller
 
 There's a matching scene in the *Main.storyboard* file, and in that scene there is a car details view controller with Navigation Controller. Go to Interface Builder for details.
 
 The `BookedCarDetailsViewController` class is a subclass of the `UIViewController`.
 */

class BookedCarDetailsViewController: UIViewController {
    
    /// Dictionary to store single car data.
    public var dictionary : Dictionary<String, Any>?
    {
        didSet{
            updateUI()
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var shortDescriptionTextView: UITextView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Update UI
    
    /**
     Updates UI for the values present against keys in dictionary
     */
    private func updateUI() {
        
        // whenever our public API dictionary is set
        // we just update our outlets using this method
        
        let nameString = dictionary?["name"] as? String ?? ""
        let shortDescriptionString = dictionary?["shortDescription"] as? String ?? ""
        let descriptionString = dictionary?["description"] as? String ?? ""
        let duration = dictionary?["duration"] as? Int ?? 0
        let datestring = dictionary?["date"] as? String ?? ""
        let timestring = dictionary?["time"] as? String ?? ""
        
        DispatchQueue.main.async() {[weak self] in //Memory cycle here
            self?.titleLabel.text = nameString
            self?.shortDescriptionTextView.text = shortDescriptionString
            self?.textView.text = descriptionString;
            self?.durationLabel.text = "\(duration)days"
            self?.imageView.image = UIImage(named: "car-placeholder")
            self?.dateAndTimeLabel.text = "Date: " + datestring + " " + "Time: " + timestring
            
            self?.view.isHidden = false
        }
        
        let imageString = dictionary?["image"] as? String? ?? ""
        
        if let imageString = imageString
        {
            let url = URL(string: imageString)
            
            let queue = DispatchQueue.global(qos: .userInitiated)
            
            queue.async{ [weak self] in //Memory cycle here
                if let imageData = try? Data(contentsOf: url!) {
                    DispatchQueue.main.async() {
                        self?.imageView?.image = UIImage(data: imageData)
                    }
                }
            }
            
            queue.resume()
        }
    }
}
