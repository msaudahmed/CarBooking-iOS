//
//  CarListTableViewCell.swift
//  CarBooking
//
//  Created by Saud Ahmed on 09.01.18.
//  Copyright © 2018 Kupferwerk. All rights reserved.
//

import UIKit

/**
 The purpose of the `CarListTableViewCell` is to provide a user interface and control to custom created cell.
 
 There's a matching cell in the car list table view controller of *Main.storyboard*.
 
 The `CarListTableViewCell` class is a subclass of the `UITableViewCell`.
 */
class CarListTableViewCell: UITableViewCell {
    /// The label for car title.
    @IBOutlet weak var carTitleLabel: UILabel!
    
    /// Variable to store a dictionary of keys "item" and "check".
    public var dictionary : Dictionary<String, Any>?
    {
        didSet{
            updateUI()
        }
    }
    
    /**
     Updates UI for the values present against keys in dictionary
     */
    private func updateUI() {
        
        // whenever our public API dictionary is set
        // we just update our outlets using this method
        
        let name = dictionary?["name"] as? String
        
        DispatchQueue.main.async() {
            self.carTitleLabel?.text = name
        }
    }
}
